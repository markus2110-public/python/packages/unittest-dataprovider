
# from sat_unittest_dataprovider import data_provider
#
#
# def data_provider_func():
#     return [
#         "data1", "data2", "data3"
#     ]
#
#
# data_provider_dict = [
#     "data1", "data2", "data3"
# ]
#
# data_provider_set = {
#     "data1", "data2", "data3"
# }
#
# data_provider_tuple = ("data1", "data2", "data3")
#
#
# class x:
#     def __init__(self):
#         self.data = [1,2,3,4]
#
#     @staticmethod
#     def b():
#         return [1, 2, 3, 4]
#
#     def a(self):
#         return self.data
#
#
# @data_provider(x.a)
# def a_func(data):
#     print(data)
#
#
# if __name__ == '__main__':
#     a_func('arg1')
